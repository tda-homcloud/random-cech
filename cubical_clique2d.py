import numpy as np
import sys


def main(shape):
    assert len(shape) == 3

    def point3d():
        for z in range(shape[0]):
            for y in range(shape[1]):
                for x in range(shape[2]):
                    yield z, y, x

    def weight(v1, v2, v3, v4, weights):
        return max([weights[(v1, v2)], weights[(v1, v3)], weights[(v2, v4)], weights[(v3, v4)]])

    def invalid(z, y, x, dz, dy, dx):
        return z + dz >= shape[0] or y + dy >= shape[1] or x + dx >= shape[2]

    vertices = dict()
    weight_1 = dict()
    weight_2 = dict()

    for (i, (z, y, x)) in enumerate(point3d()):
        vertices[(z, y, x)] = i
        print(0, 0.0, 0.0, i)

    for (z, y, x) in point3d():
        for (dz, dy, dx) in [(1, 0, 0), (0, 1, 0), (0, 0, 1)]:
            if invalid(z, y, x, dz, dy, dx):
                continue

            v1 = vertices[(z, y, x)]
            v2 = vertices[(z + dz, y + dy, x + dx)]
            weight_1[(v1, v2)] = np.random.uniform(0.0, 1.0)
            weight_2[(v1, v2)] = np.random.uniform(0.0, 1.0)
            print(1, weight_1[(v1, v2)], weight_2[(v1, v2)], v1, v2)

    ez = np.array([1, 0, 0])
    ey = np.array([0, 1, 0])
    ex = np.array([0, 0, 1])
    
    for (z, y, x) in point3d():
        for (e1, e2) in [(ex, ey), (ex, ez), (ey, ez)]:
            dz, dy, dx = e1 + e2
            if invalid(z, y, x, dz, dy, dx):
                continue

            v1 = vertices[(z, y, x)]
            v2 = vertices[tuple(e1 + [z, y, x])]
            v3 = vertices[tuple(e2 + [z, y, x])]
            v4 = vertices[tuple(e1 + e2 + [z, y, x])]
            w1 = weight(v1, v2, v3, v4, weight_1)
            w2 = weight(v1, v2, v3, v4, weight_2)
            print(1, w1, w2, v1, v4)
            print(2, w1, w2, v1, v2, v4)
            print(2, w1, w2, v1, v3, v4)


main([int(s) for s in sys.argv[1:]])

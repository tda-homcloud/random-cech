import numpy as np
import re
import sys
import bisect

# 入力
# 1番目のカラム 単体の次元
# 2番目のカラム 1番目のパラメータのレベル
# 3番目のカラム 2番目のパラメータのレベル
# 4番目以降 頂点のインデックス

# コマンドラインの意味
# N M THRESHOLD_X_1 .. THRESHOLD_X_N THRESHOLD_Y_1 .. THRESHOLD_Y_M
# 入力，出力は標準入出力を使う

# 出力
# 1番目のカラム: 単体の次元
# 2番目のカラム: ここは無効パラメータ(常に0.0)
# 3番目のカラム: n その単体が含まれる，点が増える方向のフィルトレーションのインデックス
# 4番目のカラム: m その単体が含まれる，半径によるフィルトレーションのインデックス
# 5番目以降のカラム: 頂点のインデックス，(dim + 1)個



def main(N, M, thresholds_x, thresholds_y):
    # print(N, M, thresholds_x, thresholds_y)
    for line in sys.stdin:
        line = line.strip()
        if line.startswith("#"):
            continue
        if len(line) == 0:
            continue
        dim, l1, l2, *vertices = re.split(r"\s+", line)
        # print(dim, l1, l2, vertices)
        n = bisect.bisect_right(thresholds_x, float(l1))
        m = bisect.bisect_right(thresholds_y, float(l2))
        if n < len(thresholds_x) and m < len(thresholds_y):
            print(dim, 0.0, n, m, " ".join(vertices))
        
N = int(sys.argv[1])
M = int(sys.argv[2])

main(N, M, [float(n) for n in sys.argv[3:3+N]], [float(m) for m in sys.argv[3+N:3+N+M]])

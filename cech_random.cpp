#include <gudhi/Cech_complex.h>
#include <gudhi/Rips_complex.h>
#include <gudhi/Simplex_tree.h>
#include <gudhi/distance_functions.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <array>
#include <cstdlib>
#include <sstream>
#include <random>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm/max_element.hpp>

using Point_cloud = std::vector<std::array<double, 3>>;
using Simplex_tree = Gudhi::Simplex_tree<Gudhi::Simplex_tree_options_fast_persistence>;
using Filtration_value = Simplex_tree::Filtration_value;
using Cech_complex = Gudhi::cech_complex::Cech_complex<Simplex_tree, Point_cloud>;
using Rips_complex = Gudhi::rips_complex::Rips_complex<Simplex_tree::Filtration_value>;


void show_usage_and_exit() {
  std::cout << "Usage: cech_random N M NUM_POINTS_1 .. NUM_POINTS_N R_1 .. R_M POINTCLOUD_FILE FILTRATION_FILE" << std::endl;
  std::exit(0);
}

int find_threshold_index(double r, const std::vector<double>& thresholds) {
  for (unsigned int i = 0; i < thresholds.size(); ++i) {
    if (r <= thresholds[i])
      return i;
  }
  std::cerr << "threshold error" << std::endl;
  std::exit(1);
}

// filtrationが伸びた場合は二部探索にしたほうが高速だがそんな大きなfiltrationは使わないので問題ない
int find_pc_index(int k, const std::vector<int>& cum_num_points) {
  for (unsigned int i = 0; i < cum_num_points.size(); ++i) {
    if (k < cum_num_points[i])
      return i;
  }
  
  std::cerr << "pc index error" << std::endl;
  std::exit(1);
}

template<class Complex>
void output_complex(const Complex& complex, Simplex_tree& stree, const std::vector<int>& cum_num_points, const std::vector<double>& rs,
                    std::ofstream& filtfile) {
  filtfile << "# dim birth n m v_0 .. v_dim";
#if defined(CECH_RANDOM)
  filtfile << " (CECH_RANDOM)"  << std::endl;
#elif defined(RIPS_RANDOM)
  filtfile << " (RIPS_RANDOM)"  << std::endl;
#endif
  for (auto f_simplex: stree.filtration_simplex_range()) {
    std::vector<int> vertices;
    for (auto vertex : stree.simplex_vertex_range(f_simplex)) {
      vertices.push_back(vertex);
    }

    int n = find_pc_index(*boost::max_element(vertices), cum_num_points);
    int m = find_threshold_index(stree.filtration(f_simplex), rs);
    filtfile << vertices.size() - 1 << " " << stree.filtration(f_simplex) << " "
             << n << " " << m << " ";
    for (auto vertex : vertices)
      filtfile << vertex << " ";

    filtfile << std::endl;
  }
}

int main(int argc, char *argv[])
{
  if (argc < 3)
    show_usage_and_exit();
  
  
  int N = boost::lexical_cast<int>(argv[1]);
  int M = boost::lexical_cast<int>(argv[2]);

  std::vector<int> num_points;
  std::vector<int> cum_num_points;
  std::vector<double> rs;
  
  if (argc != 1 + 2 + N + M + 2)
    show_usage_and_exit();

  for (int i = 0; i < N; ++i) {
    int k = boost::lexical_cast<int>(argv[3 + i]);
    num_points.push_back(k);
    if (i == 0)
      cum_num_points.push_back(k);
    else
      cum_num_points.push_back(k + cum_num_points.back());
  }

  for (int i = 0; i < M; ++i)
    rs.push_back(boost::lexical_cast<double>(argv[3 + N + i]));

  std::ofstream pcfile(argv[3 + N + M]);
  std::ofstream filtfile(argv[3 + N + M + 1]);
  
  Point_cloud pc;

  std::random_device seed_gen;
  std::mt19937 engine(seed_gen());
  // std::mt19937 engine(42314982u);
  std::uniform_real_distribution<> dist(-1.0, 1.0);
  
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < num_points[i]; ++j) {
      pc.push_back({dist(engine), dist(engine), dist(engine)});
    }

  for (const auto& point: pc) {
    pcfile << point[0] << " " << point[1] << " " << point[2] << std::endl;
  }
  
  Filtration_value max_radius = rs.back();

#ifdef CECH_RANDOM
  Cech_complex complex(pc, max_radius);
#else
  Rips_complex complex(pc, max_radius, Gudhi::Euclidean_distance());
#endif
  
  Simplex_tree stree;
  complex.create_complex(stree, 3 /* maxdim */);

  output_complex(complex, stree, cum_num_points, rs, filtfile);
  
  return 0;
}

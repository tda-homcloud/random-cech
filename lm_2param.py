import sys
import numpy as np

def main(n):
    edges = dict()

    for i in range(n):
        print(0, 0.0, 0.0, i)

    p = n
    for i in range(n):
        for j in range(i + 1, n):
            print(1, 0.0, 0.0, i, j)
            edges[(i, j)] = p
            p += 1

    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                print(2, np.random.uniform(0, 1.0), np.random.uniform(0, 1.0), i, j, k)



main(int(sys.argv[1]))

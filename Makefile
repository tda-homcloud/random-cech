.PHONY: all
all: cech_random rips_random random_point cech_filtration rips_filtration

cech_random: cech_random.cpp
	g++ -std=c++11 -Wall cech_random.cpp -o cech_random -I include -Wall -D CECH_RANDOM

rips_random: cech_random.cpp
	g++ -std=c++11 -Wall cech_random.cpp -o rips_random -I include -Wall -D RIPS_RANDOM

random_point: random_point.cpp
	g++ -std=c++11 -Wall random_point.cpp -o random_point -I include -Wall

cech_filtration: build_filtration.cpp
	g++ -std=c++11 -Wall build_filtration.cpp -o cech_filtration -I include -Wall -D CECH_FILTRATION

rips_filtration: build_filtration.cpp
	g++ -std=c++11 -Wall build_filtration.cpp -o rips_filtration -I include -Wall -D RIPS_FILTRATION

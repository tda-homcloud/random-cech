import numpy as np
import sys
import re


def main():
    shape, pict_1, pict_2 = read_picture()

    def invalid(z, y, x, dz, dy, dx):
        return z + dz >= shape[0] or y + dy >= shape[1] or x + dx >= shape[2]

    
    edges = [
        [(0, 0, 0)],
        [(1, 0, 0), (0, 1, 0), (0, 0, 1)],
        [(1, 1, 0), (0, 1, 1), (1, 0, 1)],
        [(1, 1, 1)],
    ]
    vertices = dict()
    for z in range(shape[0]):
        for y in range(shape[1]):
            for x in range(shape[2]):
                n = len(vertices)
                vertices[(z, y, x)] = n
                print(0, pict_1[z, y, x], pict_2[z, y, x], n)

    for z in range(shape[0]):
        for y in range(shape[1]):
            for x in range(shape[2]):
                for (dz, dy, dx) in [(1, 0, 0), (0, 1, 0), (0, 0, 1)]:
                    if invalid(z, y, x, dz, dy, dx):
                        continue
                    v1 = vertices[(z, y, x)]
                    v2 = vertices[(z + dz, y + dy, x + dx)]
                    p1 = cell_value(pict_1, z, y, x, dz, dy, dx)
                    p2 = cell_value(pict_2, z, y, x, dz, dy, dx)
                    print(1, p1, p2, " ".join(map(str, sorted([v1, v2]))))

    for z in range(shape[0]):
        for y in range(shape[1]):
            for x in range(shape[2]):
                if not invalid(z, y, x, 1, 1, 0):
                    p1 = cell_value(pict_1, z, y, x, 1, 1, 0)
                    p2 = cell_value(pict_2, z, y, x, 1, 1, 0)
                    v0 = vertices[(z, y, x)]
                    v1 = vertices[(z + 1, y, x)]
                    v2 = vertices[(z, y + 1, x)]
                    print(1, p1, p2, " ".join(map(str, sorted([v1, v2]))))
                    v3 = vertices[(z + 1, y + 1, x)]
                    print(2, p1, p2, " ".join(map(str, sorted([v0, v1, v2]))))
                    print(2, p1, p2, " ".join(map(str, sorted([v1, v2, v3]))))

                if not invalid(z, y, x, 1, 0, 1):
                    p1 = cell_value(pict_1, z, y, x, 1, 0, 1)
                    p2 = cell_value(pict_2, z, y, x, 1, 0, 1)
                    v0 = vertices[(z, y, x)]
                    v1 = vertices[(z + 1, y, x)]
                    v2 = vertices[(z, y, x + 1)]
                    print(1, p1, p2, " ".join(map(str, sorted([v1, v2]))))
                    v3 = vertices[(z + 1, y, x + 1)]
                    print(2, p1, p2, " ".join(map(str, sorted([v0, v1, v2]))))
                    print(2, p1, p2, " ".join(map(str, sorted([v1, v2, v3]))))

                if not invalid(z, y, x, 0, 1, 1):
                    p1 = cell_value(pict_1, z, y, x, 0, 1, 1)
                    p2 = cell_value(pict_2, z, y, x, 0, 1, 1)
                    v0 = vertices[(z, y, x)]
                    v1 = vertices[(z, y + 1, x)]
                    v2 = vertices[(z, y, x + 1)]
                    print(1, p1, p2, " ".join(map(str, sorted([v1, v2]))))
                    v3 = vertices[(z, y + 1, x + 1)]
                    print(2, p1, p2, " ".join(map(str, sorted([v0, v1, v2]))))
                    print(2, p1, p2, " ".join(map(str, sorted([v1, v2, v3]))))


def cell_value(pict, z, y, x, dz, dy, dx):
    return max([pict[z, y, x], pict[z + dz, y, x], pict[z, y + dy, x], pict[z + dz, y + dy, x],
                pict[z, y, x + dx], pict[z + dz, y, x + dx], pict[z, y + dy, x + dx], pict[z + dz, y + dy, x + dx]])


def boundary(z, y, x, dz, dy, dx):
    ret = []
    if dz == 1:
        ret.append((z, y, x, 0, dy, dx))
        ret.append((z + 1, y, x, 0, dy, dx))
    if dy == 1:
        ret.append((z, y, x, dz, 0, dx))
        ret.append((z, y + 1, x, dz, 0, dx))
    if dx == 1:
        ret.append((z, y, x, dz, dy, 0))
        ret.append((z, y, x + 1, dz, dy, 0))

    return ret


def read_picture():
    shape = None
    pixels_1 = []
    pixels_2 = []
    for line in sys.stdin:
        line = line.strip()
        if line.startswith("#"):
            continue
        if len(line) == 0:
            continue
        if shape:
            p1, p2 = re.split(r"\s+", line.strip())
            pixels_1.append(float(p1))
            pixels_2.append(float(p2))
        else:
            shape = [int(k) for k in re.split(r"\s+", line.strip())]
            shape.reverse()

    pict_1 = np.reshape(pixels_1, shape)
    pict_2 = np.reshape(pixels_2, shape)
    return shape, pict_1, pict_2


main()

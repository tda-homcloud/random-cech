import numpy as np
import sys


def main(n):
    weight_1 = {}
    weight_2 = {} 

    for i in range(n):
        print(0, 0.0, 0.0, i)

    for i in range(n):
        for j in range(i + 1, n):
            weight_1[(i, j)] = np.random.uniform(0.0, 1.0)
            weight_2[(i, j)] = np.random.uniform(0.0, 1.0)
            print(1, weight_1[(i, j)], weight_2[(i, j)], i, j)

    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                print(2,
                      max([weight_1[(i, j)], weight_1[(j, k)], weight_1[(i, k)]]),
                      max([weight_2[(i, j)], weight_2[(j, k)], weight_2[(i, k)]]),
                      i, j, k)
                
                  
main(int(sys.argv[1]))

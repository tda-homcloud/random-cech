#include <iostream>
#include <cstdlib>
#include <random>

#include <boost/lexical_cast.hpp>

void show_usage_and_exit() {
  std::cout << "Usage: cech_random N NUM_POINTS_1 .. NUM_POINTS_N" << std::endl;
  std::exit(0);
}

void output_full_precision_random_double(std::ostream* out, std::mt19937* rng) {
  std::uniform_real_distribution<> dist(-1.0, 1.0);
  *out << " " << boost::lexical_cast<std::string>(dist(*rng));
}

int main(int argc, char *argv[])
{
  if (argc < 3)
    show_usage_and_exit();

  int N = boost::lexical_cast<int>(argv[1]);

  if (argc != 2 + N)
    show_usage_and_exit();

  std::random_device seed_gen;
  std::mt19937 engine(seed_gen());
  // std::mt19937 engine(42314982u);

  std::vector<int> num_points;

  for (int i = 0; i < N; ++i) {
    num_points.push_back(boost::lexical_cast<int>(argv[2 + i]));
  }
  
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < num_points[i]; ++j) {
      std::cout << i;
      for (int k = 0; k < 3; ++k)
        output_full_precision_random_double(&std::cout, &engine);
      std::cout << std::endl;
    }

  return 0;
}

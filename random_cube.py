import numpy as np
import sys
import itertools
import functools

def main(shape):
    print(" ".join([str(s) for s in shape]))
    n = functools.reduce(lambda x,y: x * y, shape)

    for _ in range(n):
        print(np.random.uniform(0.0, 1.0),
              np.random.uniform(0.0, 1.0))


main([int(s) for s in sys.argv[1:]])

    


#include <gudhi/Cech_complex.h>
#include <gudhi/Rips_complex.h>
#include <gudhi/Simplex_tree.h>
#include <gudhi/distance_functions.h>

#include <iostream>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm/max_element.hpp>

using Point_cloud = std::vector<std::array<double, 3>>;
using Levels = std::vector<int>;

using Simplex_tree = Gudhi::Simplex_tree<Gudhi::Simplex_tree_options_fast_persistence>;
using Filtration_value = Simplex_tree::Filtration_value;

#ifdef CECH_FILTRATION
using Complex = Gudhi::cech_complex::Cech_complex<Simplex_tree, Point_cloud>;
#else 
using Complex = Gudhi::rips_complex::Rips_complex<Simplex_tree::Filtration_value>;
#endif

void show_usage_and_exit() {
  std::cout << "Usage: cech_random M R_1 .. R_M" << std::endl;
  std::cout << "  STDIN is used as input pointcloud with levels" << std::endl;
  std::cout << "  STDOUT is used as output 2-parameter filtration" << std::endl;
  std::exit(1);
}

void read_pointcloud(std::istream* in, Point_cloud* pointcloud, Levels* levels) {
  std::string s;
  for(int n = 1; std::getline(*in, s); ++n) {
    if (s[0] == '#')
      continue;
    
    std::vector<std::string> splitted;
    boost::algorithm::split(splitted, s, boost::is_any_of(" "));
    if (splitted.size() == 0)
      continue;

    if (splitted.size() != 4) {
      std::cerr << "Input file format error: n" << std::endl;
      std::exit(1);
    }
      
    levels->push_back(boost::lexical_cast<int>(splitted[0]));
    
    pointcloud->push_back({
        boost::lexical_cast<double>(splitted[1]),
        boost::lexical_cast<double>(splitted[2]),
        boost::lexical_cast<double>(splitted[3]),
      });
  }
}

std::vector<double> parse_args(int argc, char* argv[]) {
  if (argc < 2)
    show_usage_and_exit();

  int M = boost::lexical_cast<int>(argv[1]);

  if (argc != 2 + M)
    show_usage_and_exit();

  std::vector<double> rs;

  for (int i = 0; i < M; ++i)
    rs.push_back(boost::lexical_cast<double>(argv[2 + i]));

  return rs;
}

int find_threshold_index(double r, const std::vector<double>& thresholds) {
  for (unsigned int i = 0; i < thresholds.size(); ++i) {
    if (r <= thresholds[i])
      return i;
  }
  std::cerr << "threshold error" << std::endl;
  std::exit(1);
}

void output_complex(const Complex& complex, Simplex_tree& stree,
                    const Levels& levels, const std::vector<double>& rs) {
  std::cout << "# dim birth n m v_0 .. v_dim";
#ifdef CECH_FILTRATION
  std::cout << " (CECH_RANDOM)" << std::endl;
#else
  std::cout << " (RIPS_RANDOM)" << std::endl;
#endif
  for (auto f_simplex: stree.filtration_simplex_range()) {
    std::vector<int> vertices;
    for (auto vertex : stree.simplex_vertex_range(f_simplex)) {
      vertices.push_back(vertex);
    }

    int n = levels[*boost::max_element(vertices)];
    int m = find_threshold_index(stree.filtration(f_simplex), rs);
    std::cout << vertices.size() - 1 << " " << stree.filtration(f_simplex) << " "
             << n << " " << m << " ";
    for (auto vertex : vertices)
      std::cout << vertex << " ";

    std::cout << std::endl;
  }
}

int main(int argc, char *argv[])
{
  std::vector<double> rs = parse_args(argc, argv);
  
  Point_cloud pointcloud;
  Levels levels;
  read_pointcloud(&std::cin, &pointcloud, &levels);

  Filtration_value max_radius = rs.back();

#ifdef CECH_FILTRATION
  Complex complex(pointcloud, max_radius);
#else
  Complex complex(pointcloud, max_radius, Gudhi::Euclidean_distance());
#endif

  Simplex_tree stree;
  complex.create_complex(stree, 3 /* maxdim */);

  output_complex(complex, stree, levels, rs);
  
  return 0;
}

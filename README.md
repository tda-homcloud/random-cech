# cech_random

点を[-1,1]^3の上に一様ランダムに分布させてチェック複体を計算するプログラム．

## 依存ライブラリ

* boost library
  * Linux であれば apt や yum でインストールしてください
  * Mac であれば brew でインストールできます
  * Windows であれば boost.org からダウンロードしてヘッダを適当な場所にコピーしてください
  
## ビルドのしかた

    make all


以下の5つのファイルが得られます．

* `cech_random`
* `rips_random`
* `random_point`
* `cech_filtration`
* `rips_filtration`


## 実行のしかた (`cech_random` と `rips_random`)

以下のようにして実行します．

    ./ceck_random N M NUM_POINTS_1 .. NUM_POINTS_N R_1 .. R_M POINTCLOUD FILTRATION
    ./rips_random N M NUM_POINTS_1 .. NUM_POINTS_N R_1 .. R_M POINTCLOUD FILTRATION

引数の意味は以下の通り
* N: 点が増える方向のフィルトレーションの個数
* M: 半径が大きくなる方向のフィルトレーションの個数
* NUM_POINTS_1 .. NUM_POINTS_N: 点の個数 (それぞれの段階で増える個数)
* R_1 .. R_N: 半径パラメータ(2乗)
* POINTCLOUD: ポイントクラウドを保存するファイル
* FILTRATION: (2-パラメータ)フィルトレーションを保存するファイル

### 例

ポイントクラウド P ⊂ Q が10点，20点，半径が 0.2 0.3 0.5 の場合．

    ./cech_random 2 3 10 10 0.2 0.3 0.5 pc.txt filtration.txt

Ripsの場合も同様である．

## フィルトレーションファイルのフォーマット

まず頂点の個数は NUM_POINTS_1 + ... + NUM_POINTS_N である．そして各頂点は
0から(頂点の個数-1)までの番号がふられている．

ファイルの最初の1行目は

    # dim birth n m v_0 .. v_dim (CECH_COMPLEX)
    
というもので，これはファイルフォーマットの概略を表したコメント行である．
2行目以降が実際のデータで，各行が1つの単体を表す．

各行のデータは空白で区切られている．
* 1番目のカラム: 単体の次元
* 2番目のカラム: その単体の発生時刻
* 3番目のカラム: n その単体が含まれる，点が増える方向のフィルトレーションのインデックス
* 4番目のカラム: m その単体が含まれる，半径によるフィルトレーションのインデックス
* 5番目以降のカラム: 頂点のインデックス，(dim + 1)個

## 実行のしかた (`random_point`, `cech_filtration`, `rips_filtration`)

`random_point` は一様ランダムに `[-1,1]^3` の領域内に点をレベル付きで出力するプログラムです．
以下のように実行します．

    ./random_point N NUM_POINTS_1 .. NUM_POINTS_N

引数の意味は以下の通り

* N: 点が増える方向のフィルトレーションの個数
* NUM_POINTS_1 .. NUM_POINTS_N: 点の個数 (それぞれの段階で増える個数)

ランダム点は標準出力に出力されます．出力フォーマットは後述．

`cech_random` は `random_point` の出力からチェック複体のフィルトレーションを出力します．


    ./cech_filtration M R_1 .. R_M 

引数の意味は次の通り

* `M`: 半径が大きくなる方向のフィルトレーションの個数
* `R_1` .. `R_M`: 半径パラメータ(2乗)

入力は標準入力を用い，出力は標準出力を用います．出力フォーマットは `./cech_random` などと同じです．

### 例

ポイントクラウド P ⊂ Q が10点，20点，半径が 0.2 0.3 0.5 の場合．

    ./random_point 2 10 10 | ./cech_filtration 3 0.2 0.3 0.5 > filtration.txt

これで `cech_random` の場合と同じ出力が得られます．ポイントクラウドも取り出したい場合は `tee` などを使って

    ./random_point 2 10 10 | tee pc.txt | ./cech_filtration 3 0.2 0.3 0.5 > filtration.txt

などとすればよいでしょう．`pc.txt` の出力を手で更新して(ここでは `pc_modified.txt`というファイル名としましょう)
そこからフィルトレーションを得るためには


    ./cech_filtration 3 0.2 0.3 0.5 < pc_modified.txt > filtration.txt
    
などとすればよいでしょう．

## レベル付きポイントクラウドのフォーマット

* 最初のカラム: レベル(0から始まる)
* 2，3，4番目のカラム: その点の x y z 座標

### 例

点が増える方向のフィルトレーションが3個の場合:

    0 -0.82685678933048512 -0.090753952513462521 0.43291839383652686
    0 0.78188822237697386 0.23845884310969812 0.35546188768606513
    0 -0.96263663952803458 -0.23350282081459117 0.70457192508675726
                   :
    1 0.41681988663077885 0.35279104693080421 -0.3791123305098415
    1 0.87965184181043732 -0.6307012233258571 -0.27180420583504905
    1 0.031722672537709773 0.70521167500176696 -0.95506070549928868
                   :
    2 -0.42225564784138381 0.99034938708076159 -0.33287922280698856
    2 0.59987551896633828 -0.17726770858195429 0.90964007519760526
    2 -0.50623369378994432 -0.10299335051813518 0.15781622504449211
                   :

## 抽象単体複体 (2-parameter 2次元 Linial-Mishram)

頂点の個数nを指定して全結合グラフを考え，そこに面を貼り付けます．
面には2つのパラメータが0から1の間の乱数で割り振られます．
その2パラメータでフィルトレーションを取ります．

    python3 lm_2param.py 10 | python3 combinatorial.py 2 3 0.5 1.0 0.33 0.66 1.0
    
上の例では10が点の個数，2 3 がそれぞれの軸のフィルトレーション段数，
`0.5 1.0`が1つ目のパラメータの閾値，`0.33 0.66 1.0`が2つ目のパラメータの閾値です．

## 抽象単体複体 (2-paramter 2次元 Random Clique)

頂点の個数nを指定して，(n-1)単体を考える．
すべての辺に2つの実数(0から1まで)を割り当て，辺でフィルトレーションを考え，
3辺があらわれると同時にその面をはります．3次元以上は考えません．

    python3 random_clique.py 10 | python3 combinatorial.py 2 3 0.5 1.0 0.33 0.66 1.0
    
上の例では10が点の個数，2 3 がそれぞれの軸のフィルトレーション段数，
`0.5 1.0`が1つ目のパラメータの閾値，`0.33 0.66 1.0`が2つ目のパラメータの閾値です．


## 3次元Cubical

LxMxNの3次元ボクセルデータの各ボクセルに0から1の一様乱数を割り振り，そこから
2-parameterのフィルトレーションを生成します．

    python3 random_cube.py 2 3 4| python3 cubical.py | python3 combinatorial.py 2 3 0.5 1.0 0.33 0.66 1.0

上の例では2x3x4がデータの形で，2 3 がそれぞれの軸のフィルトレーション段数，
`0.5 1.0`が1つ目のパラメータの閾値，`0.33 0.66 1.0`が2つ目のパラメータの閾値です．

Cubcialデータの作りかたですが，各ボクセルの中心を頂点とみなします．つまりLxMxNのボクセルデータに含まれる
3次元キューブの個数は (L-1)(M-1)(N-1) です．


